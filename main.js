import './style.css';
import {Map, View} from 'ol';
import GeoJSON from 'ol/format/GeoJSON.js';
import OSM from 'ol/source/OSM';
import VectorSource from 'ol/source/Vector.js';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';

const styleFunction = function (feature) {
  const risk = feature.get('description');
  let color = 'green'; 
  if (risk.includes('ALTO')) {
    color = 'red';
  } else if (risk.includes('MEDIO')) {
    color = 'yellow';
  }
  return new Style({
    image: new CircleStyle({
      radius: 3,
      fill: new Fill({color: color}),
      stroke: new Stroke({
        color: 'white',
        width: 1
      })
    })
  }); 
};


const residuos = new VectorLayer({
  source: new VectorSource({
    url: 'residuos_urbanos.geojson',
    format: new GeoJSON({
      extractStyles: false,
    }),
  }),
  style: styleFunction,
});
  
const raster = new TileLayer({
  source: new OSM(),
});
 
const map = new Map({
  target: 'map',
  layers: [raster, residuos],
  view: new View({
    center: [-101, 22],
    projection: "EPSG:4326",
    zoom: 5
  })
});

const info = document.getElementById('info');

let currentFeature;
const displayFeatureInfo = function (pixel, target) {
  const feature = target.closest('.ol-control')
    ? undefined
    : map.forEachFeatureAtPixel(pixel, function (feature) {
        return feature;
      });
  if (feature) {
    info.style.left = pixel[0] + 'px';
    info.style.top = pixel[1] + 'px';
    if (feature !== currentFeature) {
      info.style.visibility = 'visible';
      info.innerText = feature.get('Name');
    }
  } else {
    info.style.visibility = 'hidden';
  }
  currentFeature = feature;
};

map.on('pointermove', function (evt) {
  if (evt.dragging) {
    info.style.visibility = 'hidden';
    currentFeature = undefined;
    return;
  }
  const pixel = map.getEventPixel(evt.originalEvent);
  displayFeatureInfo(pixel, evt.originalEvent.target);
});

map.on('click', function (evt) {
  displayFeatureInfo(evt.pixel, evt.originalEvent.target);
});

map.getTargetElement().addEventListener('pointerleave', function () {
  currentFeature = undefined;
  info.style.visibility = 'hidden';
});




